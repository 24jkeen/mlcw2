import context
import numpy as np
from mlcw2.image import Image
from mlcw2.icm import ICM
import matplotlib.pyplot as plt

SEED = 42
SWEEPS = 3

np.random.seed(SEED)

carl = Image('Carl.png', seed=SEED)
plt.imshow(carl.image, cmap='gray')
plt.show()

prop = 0.9
sigma = 0.5

noisy_carl = carl.add_gaussian_noise(prop, sigma)
plt.imshow(noisy_carl.image, cmap='gray')
plt.show()

salty_carl = carl.add_saltnpepper_noise(prop)
plt.imshow(salty_carl.image, cmap='gray')
plt.show()

cleanImage, er = ICM.main(noisy_carl.image, carl.image, 100)

fig = plt.figure()

ax = fig.add_subplot(141)
ax.plot(er)
ax.set_title("Similarity of Cleaned to Original")

ax2 = fig.add_subplot(142)
ax2.imshow(carl.image, cmap='gray') #clean file
ax2.set_title("Original Image")

ax3 = fig.add_subplot(143)
ax3.imshow(noisy_carl.image, cmap='gray') #dirty file
ax3.set_title("Nosiy Image")


ax4 = fig.add_subplot(144)
ax4.imshow(cleanImage, cmap='gray')
ax4.set_title("ICM Cleaned Image")

plt.show()



