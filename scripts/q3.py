import context
import numpy as np
from mlcw2.image import Image
from mlcw2.gibbs import sampler
from mlcw2.gibbs import sampler_random
import matplotlib.pyplot as plt

SEED = 42
SWEEPS = 3

np.random.seed(SEED)

carl = Image('Carl.png', seed=SEED)
plt.imshow(carl.image, cmap='gray')
plt.show()

prop = 0.9
sigma = 0.5

noisy_carl = carl.add_gaussian_noise(prop, sigma)
plt.imshow(noisy_carl.image, cmap='gray')
plt.show()

salty_carl = carl.add_saltnpepper_noise(prop)
plt.imshow(salty_carl.image, cmap='gray')
plt.show()

# clean_noisy_carl = sampler(noisy_carl, SWEEPS, seed=SEED)
# plt.imshow(clean_noisy_carl, cmap='gray')
# plt.show()
#
# clean_salty_carl = sampler(salty_carl, SWEEPS, seed=SEED)
# plt.imshow(clean_salty_carl, cmap='gray')
# plt.show()

random_noisy_carl = sampler_random(noisy_carl, SWEEPS, 0.95, seed=SEED)
plt.imshow(random_noisy_carl, cmap='gray')
plt.show()

random_salty_carl = sampler_random(salty_carl, SWEEPS, 0.95, seed=SEED)
plt.imshow(random_salty_carl, cmap='gray')
plt.show()

# print(np.array_equal(random_noisy_carl, clean_noisy_carl))
# print(np.array_equal(random_salty_carl, clean_salty_carl))
#
# print((random_noisy_carl == clean_noisy_carl).all())
# print((random_salty_carl == clean_salty_carl).all())
