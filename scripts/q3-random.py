import context
import numpy as np
from mlcw2.image import Image
from mlcw2.gibbs import sampler
from mlcw2.gibbs import sampler_random
import matplotlib.pyplot as plt

SEED = 42

np.random.seed(SEED)

carl = Image('Carl.png', seed=SEED)
plt.imshow(carl.image, cmap='gray')
plt.show()

prop = 0.9
sigma = 0.5

noisy_carl = carl.add_gaussian_noise(prop, sigma)
plt.imshow(noisy_carl.image, cmap='gray')
plt.show()

salty_carl = carl.add_saltnpepper_noise(prop)
plt.imshow(salty_carl.image, cmap='gray')
plt.show()

COVERAGE = 0.95

quality = list()
iters = list()

for sweeps in range(1, 10):
    iters.append(sweeps)
    random_noisy_carl = sampler_random(noisy_carl, sweeps, COVERAGE, seed=SEED)
    q = (np.sum(random_noisy_carl == carl.image), np.sum((random_noisy_carl * -1) == carl.image))
    quality.append(max(q))

print(quality)
print(iters)
