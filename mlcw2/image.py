import numpy as np
from scipy.misc import imread
from PIL import Image as Image_pil


class Image:

    def __init__(self, path=None, array=None, seed=None):

        if path is None and array is None:
            raise Exception('No input data.')

        if path is not None:
            self.image = imread(path)
            self.image = self.image / 256
            self.width, self.height = Image_pil.open(path).convert('LA').size

            temp_image = np.ones((self.height, self.width))
            for i in range(self.height - 1):
                for j in range(self.width - 1):
                    temp_image[i, j] = (self.image[i, j, 0] + self.image[i, j, 1] + self.image[i, j, 2]) / 3
                    if temp_image[i, j] < 0.5:
                        temp_image[i, j] = -1
                    else:
                        temp_image[i, j] = 1
            self.image = temp_image

        if array is not None:
            self.image = array
            self.height, self.width = array.shape

        if seed is not None:
            np.random.seed(seed)

    def add_gaussian_noise(self, prop, sigma):
        N = int(np.round(np.prod(self.image.shape) * prop))
        index = np.unravel_index(np.random.permutation(np.prod(self.image.shape))[1:N], self.image.shape)
        e = sigma * np.random.randn(np.prod(self.image.shape)).reshape(self.image.shape)
        noisy_image = np.copy(self.image)
        noisy_image[index] += e[index]

        return Image(array=noisy_image)

    def add_saltnpepper_noise(self, prop):

        N = int(np.round(np.prod(self.image.shape) * prop))
        index = np.unravel_index(np.random.permutation(np.prod(self.image.shape))[1:N], self.image.shape)
        noisy_image = np.copy(self.image)
        noisy_image[index] = 1 - noisy_image[index]

        return Image(array=noisy_image)
