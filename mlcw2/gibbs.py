import numpy as np
from mlcw2.image import Image


def l1(x, y):
    val = ((y - 0.5) * 2 + x) ** 2

    return val


def py_x(z1, x, y):
    val = (1 / z1) * np.exp(l1(x, y))

    return val


def e0(x, y, n, w):
    """Guess w as 1 to start. how important is it that neighbours
     are the same value? controls smoothness.

    :param x:
    :param y:
    :param n:
    :param w:
    :return:
    """

    val = 0
    for j in range(len(n)):
        val += w * x * y[n[j]]

    return val


def pxy(z1, z0, x, pixel, weight, i, j, image_array):
    assert image_array[i, j] == pixel
    h, w = image_array.shape
    n = neighbours(i, j, h, w, size=4)
    val = (1 / z1) * ((np.exp(l1(x, pixel))) / z0) * (np.exp(e0(x, image_array, n, weight)))

    return val


def neighbours(i, j, m, n, size=4):
    if size == 4:
        if i == 0 and j == 0:
            n = [(0, 1), (1, 0)]

        elif i == 0 and j == n - 1:
            n = [(0, n - 2), (1, n - 1)]
        elif i == m-1 and j == 0:
            n = [(m - 1, 1), (m - 2, 0)]
        elif i == m-1 and j == n-1:
            n = [(m - 1, n - 2), (m - 2, n - 1)]
        elif i == 0:
            n = [(0, j-1), (0, j+1), (1, j)]
        elif i == m-1:
            n = [(m - 1, j - 1), (m - 1, j + 1), (m - 2, j)]
        elif j == 0:
            n = [(i-1, 0), (i+1, 0), (i, 1)]
        elif j == n-1:
            n = [(i - 1, n - 1), (i + 1, n - 1), (i, n - 2)]
        else:
            n = [(i-1, j), (i+1, j), (i, j-1), (i, j+1)]
    if size != 4:
        raise Exception('You forgot to handle any sizes other then 4')
    return n


def sampler(im, iters, seed=None):

    if seed is not None:
        np.random.seed(seed)

    if isinstance(im, Image):
        im = im.image

    h, w = im.shape
    im_updated = np.zeros((h, w))

    for a in range(iters):
        for i in range(h):
            for j in range(w):
                pixel = im[i, j]

                pxy1 = pxy(10, 1, 1, pixel, 10, i, j, im)
                pxy2 = pxy(10, 1, -1, pixel, 10, i, j, im)
                py_x1 = py_x(1, 1, pixel)
                py_x2 = py_x(1, -1, pixel)

                px1_xy_top = (py_x1 * pxy1)
                px1_xy_bottom = (py_x1 * pxy1) + (py_x2 * pxy2)
                px1_xy = px1_xy_top / px1_xy_bottom
                t = np.random.uniform(1)

                if px1_xy < t:
                    im_updated[i, j] = 1
                else:
                    im_updated[i, j] = -1
        im = im_updated
    return im


def sampler_random(im, iters, coverage_threshold, seed=None):

    if seed is not None:
        np.random.seed(seed)

    if isinstance(im, Image):
        im = im.image

    h, w = im.shape
    im_updated = np.zeros((h, w))

    updated_pixels = set()

    for a in range(iters):
        for i in range(h):
            for j in range(w):
                i = np.random.randint(0, h)
                j = np.random.randint(0, w)

                updated_pixels.add((i, j))

                pixel = im[i, j]

                pxy1 = pxy(10, 1, 1, pixel, 10, i, j, im)
                pxy2 = pxy(10, 1, -1, pixel, 10, i, j, im)
                py_x1 = py_x(1, 1, pixel)
                py_x2 = py_x(1, -1, pixel)

                px1_xy_top = (py_x1 * pxy1)
                px1_xy_bottom = (py_x1 * pxy1) + (py_x2 * pxy2)
                px1_xy = px1_xy_top / px1_xy_bottom

                t = np.random.uniform(1)

                if px1_xy < t:
                    im_updated[i, j] = 1
                else:
                    im_updated[i, j] = -1

                if coverage_threshold < (len(updated_pixels) / (h * w)):
                    updated_pixels = set()
                    im = im_updated

    im = im_updated
    return im
