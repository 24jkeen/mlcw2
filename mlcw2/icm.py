import numpy as np
from mlcw2.image import Image
from tqdm import tqdm

class ICM:

    @staticmethod
    def l1(x, y):
        val = ((y - 0.5) * 2 + x) ** 2

        return val

    @classmethod
    def py_x(cls, z1, x, y):
        val = (1 / z1) * np.exp(cls.l1(x, y))

        return val

    @staticmethod
    def e0(x, y, n, w):
        """Guess w as 1 to start. how important is it that neighbours
         are the same value? controls smoothness.

        :param x:
        :param y:
        :param n:
        :param w:
        :return:
        """

        val = 0
        for j in range(len(n)):
            val += w * x * y[n[j]]

        return val

    @classmethod
    def pxy(cls, z1, z0, x, pixel, weight, i, j, image_array):
        assert image_array[i, j] == pixel
        h, w = image_array.shape
        n = cls.neighbours(i, j, h, w, size=4)
        val = (1 / z1) * ((np.exp(cls.l1(x, pixel))) / z0) * (np.exp(cls.e0(x, image_array, n, weight)))

        return val

    @staticmethod
    def neighbours(i, j, m, n, size=4):
        if size == 4:
            if i == 0 and j == 0:
                n = [(0, 1), (1, 0)]

            elif i == 0 and j == n - 1:
                n = [(0, n - 2), (1, n - 1)]
            elif i == m-1 and j == 0:
                n = [(m - 1, 1), (m - 2, 0)]
            elif i == m-1 and j == n-1:
                n = [(m - 1, n - 2), (m - 2, n - 1)]
            elif i == 0:
                n = [(0, j-1), (0, j+1), (1, j)]
            elif i == m-1:
                n = [(m - 1, j - 1), (m - 1, j + 1), (m - 2, j)]
            elif j == 0:
                n = [(i-1, 0), (i+1, 0), (i, 1)]
            elif j == n-1:
                n = [(i - 1, n - 1), (i + 1, n - 1), (i, n - 2)]
            else:
                n = [(i-1, j), (i+1, j), (i, j-1), (i, j+1)]
        if size != 4:
            raise Exception('You forgot to handle any sizes other then 4')
        return n

    @classmethod
    def main(cls, im, origim, iters):
        w, h = im.shape
        er = list()
        im_updated = np.copy(im)
        for a in tqdm(range(iters)):
            for i in range(w):
                for j in range(h):
                    pxy1 = cls.pxy(10, 1, 1, im[i, j], 10, i, j, im)
                    pxyNeg = cls.pxy(10, 1, -1, im[i, j], 10, i, j, im)
                    if pxy1 < pxyNeg:
                        im_updated[i, j] = -1
                    else:
                        im_updated[i, j] = 1
            im = im_updated
            q = max(np.sum(origim == im), np.sum(origim == im*-1))
            er.append(q / (w * h))
        return im, er


