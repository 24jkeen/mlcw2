clear
close all
radius = 4;
Pepperness = 0.5
RunLen = 5;

srcFiles = dir('Fig\*.jpg');  % the folder in which ur images exists
for i = 1 :length(srcFiles)
    Local1 =[];
    filename = strcat('Fig\',srcFiles(i).name);
    I = imread(filename);
    I= rgb2gray(I);
    I= imnoise(I,'salt & pepper',Pepperness);
    J = I;
    S = size(J)
    Kmedian = medfilt2(I,[radius,radius]);
    
    for j = 1:RunLen-1
        Kmedian = medfilt2(Kmedian,[radius,radius]);  
    end

    figure
    imshowpair(I,Kmedian,'montage')
    title(srcFiles(i).name)

end

load handel
sound(y,Fs)