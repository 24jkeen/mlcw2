clear
close all
radius = 10;
RunLen = 20;

srcFiles = dir('Fig\*.jpg');  % the folder in which ur images exists
for i = 1 :length(srcFiles)
    Local1 =[];
    filename = strcat('Fig\',srcFiles(i).name);
    I = imread(filename);
    I= rgb2gray(I);
    I= imnoise(I,'salt & pepper',0.8);
    
    J = I;
    S = size(J)
    
    
%     for Run = 1:RunLen    
%         for x = 1:S(1)
%             display(((Run-1)/RunLen*100)+(x/S(1)))
%             for y  = 1:S(2)
%                for dx = -radius:radius
%                    for dy = -radius:radius
%                       if (x+dx > 0)&&(x+dx < S(1))&&(y+dy > 0)&&(y+dy < S(2))&&(dx^2 + dy^2 < radius^2) 
%                           Local1(end+1) = J(x+dx,y+dy);
%                       end
%                    end
%                end
%             J2(x,y) = mean(Local1);
%             Local1 = [];
%             end
%         end
%     end
%     
%     J = J2;
    Kmedian = medfilt2(I,[radius,radius]);
    
    for j = 1:RunLen-1
        Kmedian = medfilt2(Kmedian,[radius,radius]);  
    end
%     figure
%     J = mat2gray(J);
%     imshow(J);
    figure
    imshowpair(I,Kmedian,'montage')
    title(srcFiles(i).name)

end

