import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import imread
from PIL import Image
from tqdm import tqdm

def add_gaussian_noise(im, prop, varSigma):
    N = int(np.round(np.prod(im.shape)*prop))

    index = np.unravel_index(np.random.permutation(np.prod(im.shape))[1:N], im.shape)
    e = varSigma * np.random.randn(np.prod(im.shape)).reshape(im.shape)
    im2 = np.copy(im)
    im2[index] += e[index]

    return im2

def add_saltnpepper_noise(im, prop):

    N = int(np.round(np.prod(im.shape)*prop))
    index = np.unravel_index(np.random.permutation(np.prod(im.shape))[1:N],im.shape)
    im2 = np.copy(im)
    im2[index] = 1-im2[index]
    return im2

def neighbours(i, j, M, N, size=4):
    if size == 4:
        if(i == 0 and j == 0):
            n = [(0, 1), (1, 0)]

        elif i == 0 and j == N - 1:
            n = [(0, N-2), (1, N-1)]


        elif i==M-1 and j==0:
            n=[(M-1,1), (M-2,0)]
        elif i==M-1 and j==N-1:
            n=[(M-1,N-2), (M-2,N-1)]
        elif i==0:
            n=[(0,j-1), (0,j+1), (1,j)]
        elif i==M-1:
            n=[(M-1,j-1), (M-1,j+1), (M-2,j)]
        elif j==0:
            n=[(i-1,0), (i+1,0), (i,1)]
        elif j==N-1:
            n=[(i-1,N-1), (i+1,N-1), (i,N-2)]
        else:
            n=[(i-1,j), (i+1,j), (i,j-1), (i,j+1)]
    return n
    if  size==8:
        print('Not yet implemented\n')
        return -1

def BW(File):
    im = imread(File)
    im = im/256
    image_dim = Image.open(File).convert('LA')
    w, h = image_dim.size

    print(im[1, 2, 1])
    new_im = np.ones((h, w))

    for i in range(h-1):
        for j in range(w-1):
            new_im[i, j] = im[i, j, 0] + im[ i, j, 1] + im[i, j, 2]
            if new_im[i, j] <0.5:
                new_im[i, j] = 0
            else:
                new_im[i, j] = 1

    return new_im

    return new_im

def reveal(filename,radius,RunLen) :
    Local1 = np.empty(1)  #proof by engineers induction
    #I = imread(filename);

    I = BW(filename)
    I = add_saltnpepper_noise(I, 0.8)

    J = I
    S = J.shape
    print(S)
    J2 = np.zeros(S)
    for Run in tqdm(range(0,RunLen),"Each iter") :
        for x in range(0,S[0]):
        #display(((Run - 1) / RunLen * 100) + (x / S(1)))
            for y  in range(0,S[1]):
                for dx in range(-radius,radius):
                    for dy in range(-radius,radius):
                        if (x + dx > 0) & (x + dx < S[0]) & (y + dy > 0) & (y + dy < S[1]) & (dx ^ 2 + dy ^ 2 < radius^2):
                            np.append(Local1,J[x + dx, y + dy])
                #J2[x, y] = np.mean(Local1)
                Local1 = np.empty(1)
                J2[x,y] = np.mean(Local1)
    return J2

FileName = "Pug.jpg"

prop = 0.7
varSigma = 0.1

im = imread(FileName)
im = im/256

fig = plt.figure()
ax = fig.add_subplot(231)
ax.imshow(im, cmap='gray') #Origonal file
ax.set_title("orig Img")

im2 = add_gaussian_noise(im, prop, varSigma)
ax2 = fig.add_subplot(232)
ax2.imshow(im2, cmap='gray') #Gaussia nnoise
ax2.set_title("Gaussian Noise")

im3 = add_saltnpepper_noise(im, prop)
ax3 = fig.add_subplot(233)
ax3.imshow(im3, cmap = 'gray') #SaltnPeppar
ax3.set_title("Salt'n'Peppar Noise")

new_im = BW(FileName)
ax4 = fig.add_subplot(234)
ax4.imshow(new_im, cmap='gray') #BlacknWhite
ax4.set_title("B'n'W Img")

image_dim = Image.open(FileName).convert('LA')
w, h = image_dim.size


def Average(img,w,h):
    
    for x in range(0,h-1):
        for y in range(0,w-1):
            local = neighbours(x,y,h,w,4)
            localVals = np.ones((len(local), 1))
            for i in range(0,len(local)-1):
                xCoord, yCoord = local[i]
                Vals = img[xCoord,yCoord]
                for j in range(0,len(Vals)-1):
                    Val = Val + j

                localVals[i] = Val
                if Val < 0.5:
                    localVals[i] = 1
                else:
                    localVals[i] = 0
            new_im[x,y] = np.mean(localVals)
    return new_im

New = im
for i in range(1,5):
    New = Average(New,w,h)

ax4 = fig.add_subplot(235)
ax4.imshow(New, cmap='gray') #BlacknWhite
ax4.set_title("MEan Fix Img")

new_im = BW(FileName)
ax4 = fig.add_subplot(234)
ax4.imshow(new_im, cmap='gray') #BlacknWhite
ax4.set_title("B'n'W Img")

imrevealed = 1-reveal(FileName,8,10)

ax5 = fig.add_subplot(235)
ax5.imshow(imrevealed, cmap='gray')  #filtered
ax5.set_title("All is revealed")

plt.show()
