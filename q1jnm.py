import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import imread
from PIL import Image
import cv2
from tqdm import tqdm

def neighbours(i, j, M, N, size=4):
    if size == 4:
        if (i == 0 and j == 0):
            n = [(0, 1), (1, 0)]
        elif i == 0 and j == N - 1:
            n = [(0, N - 2), (1, N - 1)]
        elif i == M - 1 and j == 0:
            n = [(M - 1, 1), (M - 2, 0)]
        elif i == M - 1 and j == N - 1:
            n = [(M - 1, N - 2), (M - 2, N - 1)]
        elif i == 0:
            n = [(0, j - 1), (0, j + 1), (1, j)]
        elif i == M - 1:
            n = [(M - 1, j - 1), (M - 1, j + 1), (M - 2, j)]
        elif j == 0:
            n = [(i - 1, 0), (i + 1, 0), (i, 1)]
        elif j == N - 1:
            n = [(i - 1, N - 1), (i + 1, N - 1), (i, N - 2)]
        else:
            n = [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)]
    return n

def add_saltnpepper_noise(im, prop):

    N = int(np.round(np.prod(im.shape)*prop))
    index = np.unravel_index(np.random.permutation(np.prod(im.shape))[1:N],im.shape)
    im2 = np.copy(im)
    im2[index] = 1-im2[index]
    return im2

def add_gaussian_noise(im, prop, varSigma):
    N = int(np.round(np.prod(im.shape)*prop))

    index = np.unravel_index(np.random.permutation(np.prod(im.shape))[1:N], im.shape)
    e = varSigma * np.random.randn(np.prod(im.shape)).reshape(im.shape)
    im2 = np.copy(im)
    im2[index] += e[index]

    return im2

def BW(File):
    im = imread(File)
    im = im/256
    image_dim = Image.open(File).convert('LA')
    w, h = image_dim.size

    #print(im[1, 2, 1])
    new_im = np.ones((h, w))

    for i in range(h-1):
        for j in range(w-1):
            new_im[i, j] = im[i, j, 0] + im[ i, j, 1] + im[i, j, 2]
            if new_im[i, j] <0.5:
                new_im[i, j] = 0
            else:
                new_im[i, j] = 1

    return new_im

im0 = BW('Pug.jpg')
im = add_gaussian_noise(im0,0.7,0.5)
dirtim=np.copy(im)

newim=np.copy(im)
s = im.shape
RunCount = 10

for i in range(RunCount):
    im=newim
    for y in range(0,s[0]-1):
        for x in range(0,s[1]-1):

            neighcoords = neighbours(x,y,s[1],s[0])
            a = len(neighcoords)
            neighvals = np.zeros(a-1)
            for neighindex in range(0,a-1):
                coords = neighcoords[neighindex]
                neighvals[neighindex] = im[coords[1],coords[0]]
            #print(neighvals)

            newim[y,x] = np.mean(neighvals)
    print(np.ndarray.sum(im0-newim))

fig = plt.figure()
ax = fig.add_subplot(141)
ax.imshow(im0, cmap='gray') #Orig file
ax.set_title("Orig")

ax2 = fig.add_subplot(142)
ax2.imshow(dirtim, cmap='gray') #dirty file
ax2.set_title("Dirty")

ax3 = fig.add_subplot(143)
ax3.imshow(newim, cmap='gray') #clean file
ax3.set_title("Clean")

ax4 = fig.add_subplot(144)
ax4.imshow(newim-im, cmap='gray') #difer file
ax4.set_title("Clean")
plt.show()


