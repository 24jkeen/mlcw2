clc
clear
close all

prop = 0.5;
varSigma = 0.1;

im = imread('Carl.png');
im = im2bw(im, 0.5);
im = double(im);
figure; imshow(im);

im2 = imnoise(im, 'gaussian', prop, varSigma);
figure; imshow(im2);

im3 = imnoise(im, 'salt & pepper', prop);
figure; imshow(im3);
%%
im_clean = restore_image(im3, varSigma, 200, 0.1, 40);
figure; imshow(im_clean);