#### Gibbs ########


import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from scipy.misc import imread
from PIL import Image

def add_gaussian_noise(im, prop, varSigma):
    N = int(np.round(np.prod(im.shape)*prop))

    index = np.unravel_index(np.random.permutation(np.prod(im.shape))[1:N], im.shape)
    e = varSigma * np.random.randn(np.prod(im.shape)).reshape(im.shape)
    im2 = np.copy(im)
    im2[index] += e[index]

    return im2

def add_saltnpepper_noise(im, prop):

    N = int(np.round(np.prod(im.shape)*prop))
    index = np.unravel_index(np.random.permutation(np.prod(im.shape))[1:N],im.shape)
    im2 = np.copy(im)
    im2[index] = 1-im2[index]
    return im2

def neighbours(i, j, M, N, size=4):
    if size == 4:
        if(i == 0 and j == 0):
            n = [(0, 1), (1, 0)]

        elif i == 0 and j == N - 1:
            n = [(0, N-2), (1, N-1)]


        elif i==M-1 and j==0:
            n=[(M-1,1), (M-2,0)]
        elif i==M-1 and j==N-1:
            n=[(M-1,N-2), (M-2,N-1)]
        elif i==0:
            n=[(0,j-1), (0,j+1), (1,j)]
        elif i==M-1:
            n=[(M-1,j-1), (M-1,j+1), (M-2,j)]
        elif j==0:
            n=[(i-1,0), (i+1,0), (i,1)]
        elif j==N-1:
            n=[(i-1,N-1), (i+1,N-1), (i,N-2)]
        else:
            n=[(i-1,j), (i+1,j), (i,j-1), (i,j+1)]
    return n
    if  size==8:
        print('Not yet implemented\n')
        return -1



prop = 0.2
varSigma = 0.5
fig = plt.figure()
#ax = fig.add_subplot(131)

File = 'carl.png'

im = imread(File)

im = im/256

image_dim = Image.open(File).convert('LA')
w, h = image_dim.size

new_im = np.ones((h, w))

for i in range(h-1):
    for j in range(w-1):
        new_im[i, j] = (im[i, j, 0] + im[ i, j, 1] + im[i, j, 2]) / 3
        if new_im[i, j] <0.5:
            new_im[i, j] = -1
        else:
            new_im[i, j] = 1

im = new_im

#plt.imshow(im, cmap='gray')


im2 = add_gaussian_noise(im, prop, varSigma)
#ax2 = fig.add_subplot(132)
plt.imshow(im2, cmap='gray')

im3 = add_saltnpepper_noise(im, prop)
#ax3 = fig.add_subplot(133)
#plt.imshow(im3, cmap = 'gray')
plt.show()

def L1(x, y):
    val = ((y - 0.5)*2 + x)**2
    
    return val

def py_x(Z1, L1, x, y):

    w, h = y.shape
    val = np.zeros([w, h]) 
    
    for i in range(w):
        for j in range(h):
            val[i, j] = (1/Z1) * np.exp(L1(x, y[i, j]))
    
    return val

#print('py|x = %f' , py_x(10, L1, -1, im2))

def E0(x, y, n, w):  #Guess w as 1 to start. how important is it that neighbours are the same value? controlls smoothness
    
    val = 0
    for j in range(len(n)):
        #print(n[j])
        val += w*x*y[n[j]] 
        
    return val 



def pxy(z1, z0, L1, E0, x, im, weight):
    w, h = im.shape
    pxy = np.zeros([w, h])
    
    for i in range(w):
        for j in range(h):
            
            n = neighbours( i, j, w, h,  size=4)
            pxy[i, j] = (1/z1) *(( np.exp( L1( x, im[i, j]))) / z0) * (np.exp( E0(x, im, n, weight  )))
            
    return pxy
    
    
def ICM(im,origim, iters):
    w, h = im.shape
    er=np.zeros(iters)
    for a in tqdm(range(iters)):
        pxy1 = pxy(10, 1, L1, E0, 1, im, 10)
        pxyNeg = pxy(10, 1, L1, E0, -1, im, 10)      
        for i in range(w):
            for j in range(h):
                if pxy1[i,j] < pxyNeg[i,j]:
                    im[i, j] = -1
                else:
                    im[i, j] = 1
        er[a-1] = np.mean(origim - im)

    return im,er

       
cleanImage,er = ICM(im3,im, 50)


fig = plt.figure()

ax = fig.add_subplot(141)
ax.plot(er)
ax.set_title("er")

ax2 = fig.add_subplot(142)
ax2.imshow(im, cmap='gray') #clean file
ax2.set_title("Clean")

ax3 = fig.add_subplot(143)
ax3.imshow(im3, cmap='gray') #dirty file
ax3.set_title("Dirty")



ax4 = fig.add_subplot(144)
ax4.imshow(cleanImage, cmap='gray')
ax4.set_title("cleaned")


plt.show()
